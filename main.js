// Initialisation des variables
window.onload = function() {
    const toDoForm  = document.getElementById('toDoListForm');
    const newTask   = document.getElementById('newTask');
    let listToDo    = document.getElementById('listToDo');
    const clearBtn  = document.getElementById('clearBtn');
    let listFromStorage = JSON.parse(window.localStorage.getItem('myList'));
    const deleteTaskBtn = document.getElementsByClassName('deleteBtn');

    if (listFromStorage != null) {
        listFromStorage.forEach(addToList);
    } else {
        listFromStorage = [];
    }

    function addToList(task) {
        let taskLi = document.createElement('li');
        taskLi.appendChild(document.createTextNode(task));
        taskLi.innerHTML += '<button class="deleteBtn" id="deleteTask">X</button>';
        listToDo.appendChild(taskLi);
        const deleteBtn = document.getElementById('deleteTask');
        deleteBtn.setAttribute('id', task);
    }

    function deleteFromList(task) {
        const el = document.getElementById(task);
        el.parentNode.remove();
    }

    function deleteTheList() {
        const list = document.getElementById("listToDo");
        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }
    }
// Creation de ma Web Socket
    const socket = new WebSocket('wss://echo.websocket.org');

// Ajout ou suppression visuel de la tâche dans ma to-do list
    socket.onmessage = message => {
        const event = JSON.parse(message.data);
        if (event[1] === 'addTask') addToList(event[0]);
        else if (event[1] === 'deleteTask') deleteFromList(event[0]);
        else if (event[1] === 'deleteList') deleteTheList();
    };

// Envoyer un message une fois que le form est submit
    toDoForm.onsubmit = e => {
        e.preventDefault();
        const task = newTask.value;
        if ((listFromStorage.includes(task)) === true) {
            alert("La tâche existe déjà");
            return false;
        } else if (task === 'Jérémie') {
            alert("La meilleure saison de Star Trek est la deuxième");
            return false;
        }
        listFromStorage.push(task);
        window.localStorage.setItem('myList', JSON.stringify(listFromStorage));
        socket.send(JSON.stringify([task, 'addTask']));
        return false;
    };

    for (let i = 0; i < deleteTaskBtn.length; i++)
        deleteTaskBtn[i].onclick = () => {
            const actualListFromStorage = JSON.parse(window.localStorage.getItem('myList'));
            const newListToStore = actualListFromStorage.filter(world => world !== deleteTaskBtn[i].id);
            window.localStorage.setItem('myList', JSON.stringify(newListToStore));
            socket.send(JSON.stringify([deleteTaskBtn[i].id, 'deleteTask']));
        };

    clearBtn.onclick = () => {
        window.localStorage.clear();
        socket.send(JSON.stringify(['empty', 'deleteList']));
    };

// Gestion d'erreur
    socket.onerror = function (error) {
        console.log('WebSocket Error: ' + error);
    };
};